import argparse
import sys
import Bio
from Bio import SeqIO
from adn import is_valid, purify


def create_parser():
    """ Declares new parser and adds parser arguments """
    program_description = ''' reading fasta file and checking sequence format '''
    parser = argparse.ArgumentParser(add_help=True, description=program_description)
    parser.add_argument('-i', '--inputfile', default=sys.stdin,
                        help="required input file in fasta format", type=str, required=True)
    return parser


def main():
    """ Main function for reading fasta file and checking sequence format """
    parser = create_parser()
    args = parser.parse_args()
    args = args.__dict__

    with open(args["inputfile"]) as my_file:
        for sequence in SeqIO.parse(my_file, "fasta"):
            if any(sequence) is True:
                seq = sequence.seq
                is_valid(seq)
                purify(seq)
                if is_valid(seq) == "FALSE":
                    print(sequence.id, "isn't valid")
                    print("number of nucleotides of", sequence.id, "is", len(sequence))
                else:
                    print(sequence.id, "is valid")
                    print("number of nucleotides of", sequence.id, "is", len(sequence))
            else:
                raise argparse.ArgumentTypeError('required input file in fasta format')


parser_ = argparse.ArgumentParser(prog="PROG")
parser_.add_argument('foo', type=main)

if __name__ == "__main__":
    main()
