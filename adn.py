nucleotides = ['a', 't', 'c', 'g']


def purify(adn):
    adn = adn.lower().replace(' ', '')  # Unify format of letters, ignore spaces
    for nuc in adn:
        if nuc not in nucleotides:
            adn = adn.replace(nuc, '')
    return adn


def is_valid(adn):
    str = "ATGC"
    for nucleotide in adn :
        if nucleotide not in str :
            return "FALSE"
    else :
        return "TRUE"




def get_valid(adn) :
    while is_valid(seq) == "FALSE":
        adn = input("Choisissez une autre séquence :")
    print("Bonne séquence")
    print("Votre séquence finale est", adn)